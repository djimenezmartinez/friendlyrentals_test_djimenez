﻿using System;
using NUnit.Framework;

namespace fr_stringcalculator.Tests
{
    [TestFixture]
    public class StringCalculatorShould
    {
        private StringCalculator _stringCalculator;

        [SetUp]
        public void SetUp()
        {
            _stringCalculator = new StringCalculator();
        }

        [TestCase("", 0)]
        [TestCase("1", 1)]
        [TestCase("2", 2)]
        public void ParseProperlyTheBaseCases(string stringNumber, int expectedNumber)
        {
            int result = _stringCalculator.Add(stringNumber);

            Assert.AreEqual(expectedNumber, result);
        }

        [TestCase("1,2", 3)]
        [TestCase("1,2,3,4", 10)]
        [TestCase("3,7,10", 20)]
        [TestCase("20,30,50", 100)]
        public void ReturnTheSumWhenSeveralNumbersAreGivenSeparatedByComma(string stringNumber, int expectedNumber)
        {
            int result = _stringCalculator.Add(stringNumber);

            Assert.AreEqual(expectedNumber, result);
        }

        [TestCase("1\n2,3", 6)]
        [TestCase("1\n2\n3\n,4", 10)]
        [TestCase("1\n2,3,4\n5", 15)]
        public void HandleNewLinesBetweenNumbersAndReturnSum(string stringNumber, int expectedNumber)
        {
            int result = _stringCalculator.Add(stringNumber);

            Assert.AreEqual(expectedNumber, result);
        }

        [TestCase("1,\n")]
        [TestCase("\n1,")]
        [TestCase("1,2\n3,4,\n5\n")]
        [TestCase("\n1,2\n3,4,\n5")]
        public void AllowOnlyNewLinesBetweenNumbers(string stringNumber)
        {
            ArgumentException exception = Assert.Throws<ArgumentException>(() => _stringCalculator.Add(stringNumber));
            
            Assert.AreEqual(exception.Message, "invalid input");
        }

        [TestCase("//;\n1;2;3", 6)]
        [TestCase("//;;\n1;;2;;3", 6)]
        [TestCase("//;\n1;2;3\n4;5", 15)]
        [TestCase("//bla\n1bla2bla3\n4bla5", 15)]
        [TestCase("//t\n1t2t3\n4t5", 15)]
        public void SupportNewDelimiterIfGiven(string stringNumber, int expectedNumber)
        {
            int result = _stringCalculator.Add(stringNumber);

            Assert.AreEqual(expectedNumber, result);
        }

        [TestCase("//;;\n1;2;3)")]
        [Ignore("Ignoring until I find out the proper Regex to make this specific case throw an exception")]
        public void ThrowIfDifferentButCloselySimilarDelimiterIsFound(string stringNumbers)
        {
            ArgumentException exception = Assert.Throws<ArgumentException>(() => _stringCalculator.Add(stringNumbers));

            Assert.AreEqual(exception.Message, "invalid delimiter");
        }

        [TestCase("//-\n-1--2--3-4)", "-2,-3")]
        [Ignore("This is a case that maybe should work, stripping properly negative numbers and non-negative ones based on a dash. " +
                "I decided to forbid the use of a dash as delimiter explicitly in code, but if required from the interviewer I could make this" +
                "work with some more time and googling")]
        public void ThrowExceptionIfNegativeNumbersAreSuppliedAndDashIsSupplied(string stringNumbers, string negativeNumbers)
        {
            var exception = Assert.Throws<ArgumentException>(() => _stringCalculator.Add(stringNumbers));

            Assert.AreEqual(exception.Message, $"negatives not allowed: {negativeNumbers}");
        }

        [TestCase("//;\n1,2,3")]
        [TestCase("//bla\n1blas2bla3")]
        [TestCase("//\n123", Description = "Empty delimiter should not be supplied")]
        [TestCase("//,\n1;2;3")]
        public void ThrowIfDifferentDelimiterIsUsed(string stringNumbers)
        {
            ArgumentException exception = Assert.Throws<ArgumentException>(() => _stringCalculator.Add(stringNumbers));

            Assert.AreEqual(exception.Message, "invalid delimiter");
        }

        [TestCase("//4\n14243")]
        [TestCase("//4,\n14,24,3")]
        [TestCase("//,4,\n1,4,2,4,3")]
        [TestCase("//-\n1-4")]
        public void ThrowIfNumberOrDashIsUsedAsDelimiter(string stringNumbers)
        {
            ArgumentException exception = Assert.Throws<ArgumentException>(() => _stringCalculator.Add(stringNumbers));

            Assert.AreEqual(exception.Message, "number or dash as delimiter");
        }

        [TestCase("//;\n1;2;-3;4;-7", "-3,-7")]
        [TestCase("//;\n-1", "-1")]
        [TestCase("-1", "-1")]
        [TestCase("-2\n-3,4", "-2,-3")]
        public void ThrowExceptionIfNegativeNumbersAreSupplied(string stringNumbers, string negativeNumbers)
        {
            var exception = Assert.Throws<ArgumentException>(() => _stringCalculator.Add(stringNumbers));

            Assert.AreEqual(exception.Message, $"negatives not allowed: {negativeNumbers}");
        }
    }
}
