using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace fr_stringcalculator
{
    public class StringCalculator
    {
        public const string DEFAULT_DELIMITER = ",";
        
        private readonly Regex _delimiterRegex = new Regex("^//(?<separator>.*?)\n.*", RegexOptions.Compiled);
        private readonly Regex _numberRegex = new Regex("[0-9\\-]", RegexOptions.Compiled);
        private readonly Regex _delimiterRemoveRegex = new Regex("^//.*?\n", RegexOptions.Compiled);

        public int Add(string numbers)
        {
            if (numbers.EndsWith("\n") || numbers.StartsWith("\n"))
            {
                throw new ArgumentException("invalid input");
            }

            string delimiter = GetDelimiter(numbers);
            string strippedNumbers = StripDelimiterFrom(numbers);
            
            CheckInvalidDelimiter(strippedNumbers, delimiter);

            IEnumerable<int> parsedNumbers = strippedNumbers
                .Split(new[] {delimiter, "\n"}, StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse)
                .ToList();

            CheckForNegativeNumbers(parsedNumbers);

            return parsedNumbers.Sum();
        }

        private static void CheckForNegativeNumbers(IEnumerable<int> parsedNumbers)
        {
            // NOTE for the reviewer: This may be done as well with a regular expression
            List<int> negativeNumbers = parsedNumbers.Where(x => x < 0).ToList();
            if (negativeNumbers.Count > 0)
            {
                throw new ArgumentException($"negatives not allowed: {string.Join(",", negativeNumbers)}");
            }
        }

        private void CheckInvalidDelimiter(string strippedNumbers, string delimiter)
        {
            if (_numberRegex.IsMatch(delimiter))
            {
                throw new ArgumentException("number or dash as delimiter");
            }
            
            if (Regex.IsMatch(strippedNumbers, $"[^(?={delimiter}\\-0-9\\n]") || string.IsNullOrEmpty(delimiter))
            {
                throw new ArgumentException("invalid delimiter");
            }
        }

        private string StripDelimiterFrom(string numbers)
        {
            return _delimiterRemoveRegex.Replace(numbers, string.Empty);
        }

        private string GetDelimiter(string numbers)
        {
            Match match = _delimiterRegex.Match(numbers);
            return match.Groups["separator"].Success ? match.Groups["separator"].Value : DEFAULT_DELIMITER;
        }
    }
}